<?php

namespace App\Controllers;

class Soap extends BaseController
{
    public function getDataSources(): string
    {
        $coreService = model('App\Models\Soap\CoreService');
        $soapClient = model('App\Models\Soap\DataSourceService');
        $authToken = $coreService->login();
        $soapClient->authenticate($authToken);
        $items = $soapClient->fetchDataSourceAll();
        return view('soap', array('title' => 'Datasources', 'items' => $items));
    }

    public function getStudenten(): string
    {
        $coreService = model('App\Models\Soap\CoreService');
        $soapClient = model('App\Models\Soap\DataSourceService');
        $authToken = $coreService->login();
        $soapClient->authenticate($authToken);
        $items = $soapClient->fetchDataSourceFields(47);
        return view('soap', array('title' => 'Studenten', 'items' => $items));
    }
}
