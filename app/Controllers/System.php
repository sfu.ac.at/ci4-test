<?php

namespace App\Controllers;

class System extends BaseController
{
    /**
     * Checks the existence of schemas, tables, columns, indexes
     * and datatype of the columns in pgmodeler dbm-file and the database.
     * Only data with problems in results
     */
    public function getDbModel(): string
    {
        $results = array();
        $pgmodel = model('App\Models\System\PGModel');
        $pgmodel->checkSchema($results);
        $pgmodel->checkIndex($results);
        $pgmodel->checkTableColumn($results);
        return view('system', array('title' => 'DbModel', 'items' => $results));
    }
}
