<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Shield\Entities\User;
use CodeIgniter\API\ResponseTrait;

class Usermanagement extends BaseController
{
    use ResponseTrait;

    public function __construct()
    {
        $this->model = auth()->getProvider();
    }

    public function getIndex()
    {
        $data = [
            'users' => $this->model->findAll(),
            // 'users' => $this->model->withIdentities()->findAll(),
            'title' => 'Usermanagement',
        ];
        return view('userManagement', $data);
    }

    private function create($data)
    {
        $user = new User($data);
        if (!$this->model->save($user)) {
            $errors = $this->model->errors();
            return $this->failValidationErrors($errors);
        }
        $result = $this->model->findById($this->model->getInsertID());
        $this->model->addToDefaultGroup($result);
        return $this->respondCreated($result);
    }

    private function update($data)
    {
        $user = $this->model->findById($data['id']);
        $user->fill($data);
        if (!$this->model->save($user)) {
            $errors = $this->model->errors();
            return $this->failValidationErrors($errors);
        }
        $result = $this->model->findById($data['id']);
        return $this->respond($result);
    }

    public function postIndex()
    {
        $data = $this->request->getJSON(true);
        if(isset($data['id'])) {
            return $this->update($data);
        }
        return $this->create($data);
    }

    public function deleteIndex($id)
    {
        if((int)$id === auth()->id()) {
            return $this->failForbidden('Not allowed to delete current logged in user');
        }
        $result = $this->model->delete($id, true);
        return $this->respondDeleted($result);
    }
}
