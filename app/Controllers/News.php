<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;

class News extends BaseController
{
    use ResponseTrait;

    public function __construct()
    {
        $this->model = model('App\Models\News');
    }

    public function getIndex()
    {
        $data = [
            'news' => $this->model->findAll(),
            'title' => 'News',
        ];
        return view('news', $data);
    }

    public function postIndex()
    {
        $data = $this->request->getJSON(true);
        if (!$this->model->save($data)) {
            $errors = $this->model->errors();
            return $this->failValidationErrors($errors);
        }
        $id = isset($data['id']) ? $data['id'] : $this->model->getInsertID();
        $result = $this->model->find($id);
        return isset($data['id']) ? $this->respond($result) : $this->respondCreated($result);
    }

    public function deleteIndex($id)
    {
        $result = $this->model->delete($id);
        return $this->respondDeleted($result);
    }
}
