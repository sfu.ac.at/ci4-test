INSERT INTO
  news (title, body)
VALUES
  (
    'Elvis sighted',
    'Elvis was sighted at the Podunk internet cafe. It looked like he was writing a CodeIgniter app.'
  ),
  ('Say it isn''t so!', 'Scientists conclude that some programmers have a sense of humor.'),
  ('Caffeination, Yes!', 'World''s largest coffee shop open onsite nested coffee shop for staff only.');