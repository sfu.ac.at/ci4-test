<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class NewsSeeder extends Seeder
{
    public function run()
    {
        $sql = file_get_contents(dirname(__FILE__).'/news-data.sql');
        $this->db->query($sql);
    }
}
