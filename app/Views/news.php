<?= $this->extend('templates/twbs_dashboard') ?>
<?= $this->section('title') ?>CodeIgniter Tutorial<?= $this->endSection() ?>
<?= $this->section('heading') ?><?= esc($title) ?><?= $this->endSection() ?>

<?= $this->section('toolbar') ?>
<form class="me-2">
    <input
        v-model="state.filterKey"
        id="search"
        type="search"
        class="form-control"
        placeholder="Search"
    />
</form>
<button
    type="button"
    class="btn btn-primary"
    @click="newItem"
    data-bs-toggle="offcanvas"
    data-bs-target="#itemEditor"
>
    New
</button>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<data-grid v-bind="state" @select-item="selectItem"></data-grid>
<div id="itemEditor" class="offcanvas offcanvas-end" tabindex="-1">
    <div v-if="state.selectedItem" class="offcanvas-header bg-body-tertiary">
        <h5 class="offcanvas-title me-auto">{{ state.selectedItem.title }}</h5>
        <button
            type="button"
            class="nav-link fs-5"
            data-bs-dismiss="offcanvas"
            data-bs-target="#itemEditor"
            aria-label="Close"
        >
            <i class="bi bi-x"></i>
        </button>
    </div>
    <div v-if="state.selectedItem" class="offcanvas-body">
        <form @submit.prevent="saveItem">
            <div class="form-floating mb-3">
                <input
                    v-model="state.selectedItem.title"
                    id="title"
                    type="text"
                    class="form-control"
                    placeholder="Title"
                    required
                />
                <label for="title">Title</label>
            </div>
            <div class="form-floating mb-3">
                <textarea
                    v-model="state.selectedItem.body"
                    id="body"
                    class="form-control"
                    style="height: 100px"
                    placeholder="Text"
                    required
                ></textarea>
                <label for="body">Text</label>
            </div>
            <button
                type="submit"
                class="btn btn-primary me-2"
            >
                Save
            </button>
            <button
                v-if="state.selectedItem.id"
                type="button"
                class="btn btn-danger me-2"
                @click="deleteItem"
            >
                Delete
            </button>
            <button
                type="button"
                class="btn btn-secondary"
                data-bs-dismiss="offcanvas"
                data-bs-target="#itemEditor"
            >
                Cancel
            </button>
        </form>
        <div
            v-show="state.errorMessage"
            class="alert alert-danger mt-3"
            role="alert"
            v-text="state.errorMessage"
        >
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('end_of_body') ?>
<script type="module">
    import DataGrid from '/js/components/DataGrid.js';
    import { fetchPost, fetchDelete } from '/js/services/api.js';
    const { createApp, reactive } = Vue;
    const currentPath = location.pathname;
    const app = createApp({
        components: { DataGrid },
        setup() {
            const state = reactive({
                data: <?= json_encode((isset($news)) ? $news : []) ?>,
                filterKey: '',
                selectedItem: null,
                errorMessage: ''
            })
            let cachedItem = null;
            function getSelectedIndex() {
                return state.data.indexOf(cachedItem);
            }
            function newItem() {
                state.errorMessage = '';
                state.selectedItem = { title: '', body: '' };
                itemEditorOffcanvas.show();
            }
            function selectItem(item) {
                state.errorMessage = '';
                cachedItem = item;
                state.selectedItem = { ...item };
                itemEditorOffcanvas.show();
            }
            function setError(data) {
                state.errorMessage = data.message || data.messages || data;
            }
            async function saveItem() {
                const result = await fetchPost(currentPath, state.selectedItem);
                if(result.error) return setError(result.error);
                if(state.selectedItem.id) state.data.splice(getSelectedIndex(), 1, result.data);
                else state.data.push(result.data);
                itemEditorOffcanvas.hide();
            }
            async function deleteItem() {
                const result = await fetchDelete(currentPath + '/' + state.selectedItem.id);
                if(result.error) return setError(result.error);
                state.data.splice(getSelectedIndex(), 1);
                itemEditorOffcanvas.hide();
            }
            return {
                state,
                newItem,
                selectItem,
                saveItem,
                deleteItem
            };
        }
    });
    app.mount('#vue-app-container');
    const itemEditorElement = document.querySelector('#itemEditor');
    const itemEditorOffcanvas = new bootstrap.Offcanvas(itemEditorElement);
</script>
<?= $this->endSection() ?>
