<?= $this->extend('templates/twbs_dashboard') ?>

<?= $this->section('title') ?>Welcome to CodeIgniter 4!<?= $this->endSection() ?>
<?= $this->section('heading') ?>Welcome to CodeIgniter <?= CodeIgniter\CodeIgniter::CI_VERSION ?><?= $this->endSection() ?>
<?= $this->section('content') ?>
<section>
    <h2>About this page</h2>
    <p>The page you are looking at is being generated dynamically by CodeIgniter.
        If you would like to edit this page you will find it located at:
        <code>app/Views/welcome_message.php</code>. The corresponding controller
        for this page can be found at: <code>app/Controllers/Home.php</code>.</p>
</section>
<section>
    <h2>Learn</h2>
    <p>The User Guide contains an introduction, tutorial, a number of "how to"
        guides, and then reference documentation for the components that make up
        the framework. Check the <a href="https://codeigniter.com/user_guide/"
        target="_blank">User Guide</a>!</p>
</section>
<section>
    <h2>Discuss</h2>
    <p>CodeIgniter is a community-developed open source project, with several
        venues for the community members to gather and exchange ideas. View all
        the threads on <a href="https://forum.codeigniter.com/"
        target="_blank">CodeIgniter's forum</a>, or <a href="https://join.slack.com/t/codeigniterchat/shared_invite/zt-rl30zw00-obL1Hr1q1ATvkzVkFp8S0Q"
        target="_blank">chat on Slack</a>!</p>
</section>
<section>
    <h2>Contribute</h2>
    <p>CodeIgniter is a community driven project and accepts contributions
        of code and documentation from the community. Why not
        <a href="https://codeigniter.com/contribute" target="_blank">
        join us</a>?</p>
</section>
<footer class="py-3 border-top mt-4 text-body-secondary">
    <p class="mb-0">Page rendered in {elapsed_time} seconds using {memory_usage} MB of memory. &copy; <?= date('Y') ?></p>
</footer>
<?= $this->endSection() ?>
