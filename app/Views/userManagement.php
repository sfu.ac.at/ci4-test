<?= $this->extend('templates/twbs_dashboard') ?>
<?= $this->section('title') ?><?= $title ?><?= $this->endSection() ?>
<?= $this->section('heading') ?><?= $title ?><?= $this->endSection() ?>

<?= $this->section('toolbar') ?>
<form class="me-2">
    <input
        v-model="state.filterKey"
        id="search"
        type="search"
        class="form-control"
        placeholder="Suche"
    />
</form>
<button
    type="button"
    class="btn btn-primary"
    @click="newItem"
    data-bs-toggle="offcanvas"
    data-bs-target="#itemEditor"
>
    Hinzufügen
</button>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<data-grid v-bind="state" @select-item="selectItem"></data-grid>
<div id="itemEditor" class="offcanvas offcanvas-end" tabindex="-1">
    <div v-if="state.selectedItem" class="offcanvas-header bg-body-tertiary">
        <h5 class="offcanvas-title me-auto">{{ state.selectedItem.username }}</h5>
        <button
            type="button"
            class="nav-link fs-5"
            data-bs-dismiss="offcanvas"
            data-bs-target="#itemEditor"
            aria-label="Close"
        >
            <i class="bi bi-x"></i>
        </button>
    </div>
    <div v-if="state.selectedItem" class="offcanvas-body">
        <form @submit.prevent="saveItem">
            <div class="form-floating mb-3">
                <input
                    v-model="state.selectedItem.username"
                    id="username"
                    type="text"
                    class="form-control"
                    placeholder="Username"
                    required
                />
                <label for="username">Username</label>
            </div>
            <div v-if="!state.selectedItem.id" class="form-floating mb-3">
                <input
                    v-model="state.selectedItem.email"
                    id="email"
                    type="email"
                    class="form-control"
                    placeholder="Email"
                    required
                />
                <label for="email">Email</label>
            </div>
            <div v-if="!state.selectedItem.id" class="form-floating mb-3">
                <input
                    v-model="state.selectedItem.password"
                    id="password"
                    type="password"
                    class="form-control"
                    placeholder="Password"
                    required
                />
                <label for="password">Password</label>
            </div>
            <div class="form-check mb-3">
                <input
                    v-model="state.selectedItem.active"
                    id="active"
                    type="checkbox"
                    class="form-check-input"
                />
                <label class="form-check-label" for="active">Active</label>
            </div>
            <button
                type="submit"
                class="btn btn-primary me-2"
            >
                Speichern
            </button>
            <button
                v-if="state.selectedItem.id"
                type="button"
                class="btn btn-danger me-2"
                @click="deleteItem"
            >
                Löschen
            </button>
            <button
                type="button"
                class="btn btn-secondary"
                data-bs-dismiss="offcanvas"
                data-bs-target="#itemEditor"
            >
                Abbrechen
            </button>
        </form>
        <div
            v-show="state.errorMessage"
            class="alert alert-danger mt-3"
            role="alert"
            v-text="state.errorMessage"
        >
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('end_of_body') ?>
<script type="module">
    import DataGrid from '/js/components/DataGrid.js';
    import { fetchPost, fetchDelete } from '/js/services/api.js';
    const { createApp, reactive } = Vue;
    const currentPath = location.pathname;
    const app = createApp({
        components: { DataGrid },
        setup() {
            const state = reactive({
                data: <?= json_encode(isset($users) ? $users : []) ?>,
                headers: [
                    { key: 'id', label: 'ID' },
                    { key: 'username', label: 'User Name' },
                    { key: 'active', label: 'Active', sortable: false }
                ],
                filterKey: '',
                selectedItem: null,
                errorMessage: ''
            })
            let cachedItem = null;
            function getSelectedIndex() {
                return state.data.indexOf(cachedItem);
            }
            function newItem() {
                state.errorMessage = '';
                state.selectedItem = { username: '', email: '', password: '', active: false };
                itemEditorOffcanvas.show();
            }
            function selectItem(item) {
                state.errorMessage = '';
                cachedItem = item;
                state.selectedItem = { ...item };
                itemEditorOffcanvas.show();
            }
            function setError(data) {
                state.errorMessage = data.message || data.messages || data;
            }
            async function saveItem() {
                const result = await fetchPost(currentPath, state.selectedItem);
                if(result.error) return setError(result.error);
                if(state.selectedItem.id) state.data.splice(getSelectedIndex(), 1, result.data);
                else state.data.push(result.data);
                itemEditorOffcanvas.hide();
            }
            async function deleteItem() {
                const result = await fetchDelete(currentPath + '/' + state.selectedItem.id);
                if(result.error) return setError(result.error);
                state.data.splice(getSelectedIndex(), 1);
                itemEditorOffcanvas.hide();
            }
            return {
                state,
                newItem,
                selectItem,
                saveItem,
                deleteItem
            };
        }
    });
    app.mount('#vue-app-container');
    const itemEditorElement = document.querySelector('#itemEditor');
    const itemEditorOffcanvas = new bootstrap.Offcanvas(itemEditorElement);
</script>
<?= $this->endSection() ?>
