<?= $this->extend('templates/twbs_dashboard') ?>
<?= $this->section('title') ?><?= $title ?><?= $this->endSection() ?>
<?= $this->section('heading') ?><?= $title ?><?= $this->endSection() ?>

<?= $this->section('content') ?>
<?php if (isset($items) && count($items) > 0): ?>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr><th><?php echo implode('</th><th>', array_keys((array) current($items))); ?></th></tr>
            </thead>
            <tbody>
                <?php foreach ($items as $item): ?>
                    <tr><td><?php echo implode('</td><td>', (array) $item); ?></td></tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
<?= $this->endSection() ?>