<?= $this->extend('templates/twbs_dashboard_base') ?>

<?= $this->section('title') ?><?= lang('Auth.login') ?> <?= $this->endSection() ?>

<?= $this->section('main') ?>

<main class="container d-flex justify-content-center flex-grow-1 align-items-center">
    <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4 p-3 pb-5">
        <h1 class="h3 mb-3 fw-normal"><?= lang('Auth.login') ?></h1>
        
        <?php if (session('error') !== null) : ?>
            <div class="alert alert-danger" role="alert"><?= session('error') ?></div>
        <?php elseif (session('errors') !== null) : ?>
            <div class="alert alert-danger" role="alert">
                <?php if (is_array(session('errors'))) : ?>
                    <?php foreach (session('errors') as $error) : ?>
                        <?= $error ?>
                        <br>
                    <?php endforeach ?>
                <?php else : ?>
                    <?= session('errors') ?>
                <?php endif ?>
            </div>
        <?php endif ?>

        <?php if (session('message') !== null) : ?>
            <div class="alert alert-success" role="alert"><?= session('message') ?></div>
        <?php endif ?>

        <form action="<?= url_to('login') ?>" method="post">
            <?= csrf_field() ?>

                    <!-- Username -->
            <div class="form-floating mb-3">
                <input
                    type="username"
                    class="form-control"
                    id="floatingUsernameInput"
                    name="username"
                    inputmode="username"
                    autocomplete="username"
                    placeholder="<?= lang('Auth.username') ?>"
                    value="<?= old('username') ?>"
                    required
                >
                <label for="floatingUsernameInput"><?= lang('Auth.username') ?></label>
            </div>

                    <!-- Password -->
            <div class="form-floating mb-3">
                <input
                    type="password"
                    class="form-control"
                    id="floatingPasswordInput"
                    name="password"
                    inputmode="text"
                    autocomplete="current-password"
                    placeholder="<?= lang('Auth.password') ?>"
                    required
                >
                <label for="floatingPasswordInput"><?= lang('Auth.password') ?></label>
            </div>

                    <!-- Remember me -->
            <?php if (setting('Auth.sessionConfig')['allowRemembering']): ?>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="checkbox" name="remember" class="form-check-input" <?php if (old('remember')): ?> checked<?php endif ?>>
                        <?= lang('Auth.rememberMe') ?>
                    </label>
                </div>
            <?php endif; ?>

            <div class="d-grid col-12 col-md-8 mx-auto m-3">
                <button type="submit" class="btn btn-primary btn-block"><?= lang('Auth.login') ?></button>
            </div>

            <?php if (setting('Auth.allowMagicLinkLogins')) : ?>
                <p class="text-center">
                    <?= lang('Auth.forgotPassword') ?> <a href="<?= url_to('magic-link') ?>"><?= lang('Auth.useMagicLink') ?></a>
                </p>
            <?php endif ?>

            <?php if (setting('Auth.allowRegistration')) : ?>
                <p class="text-center"><?= lang('Auth.needAccount') ?> <a href="<?= url_to('register') ?>"><?= lang('Auth.register') ?></a></p>
            <?php endif ?>

        </form>
    </div>
</main>

<?= $this->endSection() ?>
