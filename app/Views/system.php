<?= $this->extend('templates/twbs_dashboard') ?>
<?= $this->section('title') ?><?= esc($title) ?><?= $this->endSection() ?>
<?= $this->section('heading') ?><?= esc($title) ?><?= $this->endSection() ?>

<?= $this->section('toolbar') ?>
<form>
    <input
        v-model="state.filterKey"
        id="search"
        type="search"
        class="form-control"
        placeholder="Search"
    />
</form>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<data-grid v-bind="state"></data-grid>
<?= $this->endSection() ?>

<?= $this->section('end_of_body') ?>
<script type="module">
    import DataGrid from '/js/components/DataGrid.js';
    const { createApp, reactive } = Vue;
    const app = createApp({
        components: { DataGrid },
        setup() {
            const state = reactive({
                data: <?= json_encode(isset($items) ? $items : []) ?>,
                filterKey: ''
            })
            return { state };
        }
    });
    app.mount('#vue-app-container');
</script>
<?= $this->endSection() ?>
