<?= $this->extend('templates/twbs_dashboard_base') ?>

<?= $this->section('end_of_head') ?>
<style>
  @media (min-width: 992px) {
    .sidebar {
      position: sticky;
      top: 62px;
      height: calc(100vh - 62px);
      overflow: auto;
    }
  }
</style>
<?= $this->endSection() ?>

<?= $this->section('header') ?>
<header class="navbar navbar-expand sticky-top bg-body-tertiary">
  <div class="container-xxl px-2">
  <div class="navbar-nav d-lg-none">
    <button
      type="button"
      class="nav-link fs-5"
      data-bs-toggle="offcanvas"
      data-bs-target="#sidebarMenu"
      aria-controls="sidebarMenu"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <i class="bi bi-list"></i>
    </button>
  </div>
  <a class="navbar-brand p-2 me-auto" href="/">ci4test</a>
  <div class="navbar-nav">
    <button
      type="button"
      id="bd-theme"
      class="nav-link fs-5"
      aria-label="Toggle theme"
    >
      <i class="bi bi-sun-fill" data-bs-theme-value="light"></i>
      <i class="bi bi-moon-stars-fill" data-bs-theme-value="dark"></i>
    </button>
    </div>
  </div>
</header>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-xxl">
  <div class="row">
    <aside class="sidebar col-lg-3 px-0">
      <?php include('twbs_dashboard_sidebar.php'); ?>
    </aside>
    <main id="vue-app-container" class="col-lg-9 px-3">
      <div class="d-flex justify-content-between flex-wrap flex-lg-nowrap align-items-center py-3">
        <h1 class="me-2"><?= $this->renderSection('heading') ?></h1>
        <div class="btn-toolbar mb-2 mb-lg-0"><?= $this->renderSection('toolbar') ?></div>
      </div>
      <?= $this->renderSection('content') ?>
    </main>
  </div>
</div>
<?= $this->endSection() ?>

