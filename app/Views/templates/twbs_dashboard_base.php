<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?= $this->renderSection('title') ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/vendor/bootstrap-icons/font/bootstrap-icons.min.css">
  <style>
    [data-bs-theme="light"] #bd-theme .bi-sun-fill,
    [data-bs-theme="dark"] #bd-theme .bi-moon-stars-fill {
      display: none;
    }
  </style>
  <?= $this->renderSection('end_of_head') ?>
</head>
<body class="vh-100 d-flex flex-column" data-bs-theme="dark">
  <?= $this->renderSection('header') ?>
  <?= $this->renderSection('main') ?>
  <script>
    (() => {
      // https://getbootstrap.com/docs/5.3/customize/color-modes/#javascript
      'use strict'
      const storedTheme  = localStorage.getItem('theme')
      const setTheme = theme => document.body.setAttribute('data-bs-theme', theme)
      if (storedTheme) setTheme(storedTheme)
      document.querySelectorAll('[data-bs-theme-value]').forEach(toggle => {
        toggle.addEventListener('click', () => {
          const theme = toggle.getAttribute('data-bs-theme-value')
          localStorage.setItem('theme', theme)
          setTheme(theme)
        })
      })
    })()
  </script>
  <script src="/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="/vendor/vue/dist/vue.global.prod.js"></script>
  <?= $this->renderSection('end_of_body') ?>
</body>
</html>
