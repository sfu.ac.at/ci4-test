<?php
// https://codeigniter.com/user_guide/libraries/uri.html
$uri = service('uri')->getSegments();
$uri0 = $uri[0] ?? '';
$uri1 = $uri[1] ?? '';
$uri2 = $uri[2] ?? '';
?>
<div id="sidebarMenu" class="offcanvas-lg offcanvas-start" tabindex="-1" aria-labelledby="sidebarMenuLabel">
  <div class="offcanvas-header bg-body-tertiary">
    <button
      type="button"
      class="nav-link fs-5"
      data-bs-dismiss="offcanvas"
      data-bs-target="#sidebarMenu"
      aria-label="Close"
    >
      <i class="bi bi-x"></i>
    </button>
  </div>
  <div class="offcanvas-body px-0 py-3 flex-column">
    <h6 class="py-2 px-3 mb-0 fw-normal text-secondary">Soap</h6>
    <nav class="list-group list-group-flush mb-2">
      <a
        href="/Soap/DataSources"
        class="list-group-item list-group-item-action border-bottom-0 d-flex align-items-center gap-2
        <?= ($uri1 == "DataSources") ? ' active' : '' ?>"
      >
        <i class="bi bi-database-gear"></i>
        DataSources
      </a>
      <a
        href="/Soap/Studenten"
        class="list-group-item list-group-item-action border-bottom-0 d-flex align-items-center gap-2
        <?= ($uri1 == "Studenten") ? ' active' : '' ?>"
      >
        <i class="bi bi-people"></i>
        Studenten
      </a>
    </nav>
    <h6 class="py-2 px-3 mb-0 fw-normal text-secondary">System</h6>
    <nav class="list-group list-group-flush mb-2">
      <a
        href="/System/DbModel"
        class="list-group-item list-group-item-action border-bottom-0 d-flex align-items-center gap-2
        <?= ($uri1 == "DbModel") ? ' active' : '' ?>"
      >
        <i class="bi bi-database-check"></i>
        DbModel
      </a>
      <a
        href="/Usermanagement"
        class="list-group-item list-group-item-action border-bottom-0 d-flex align-items-center gap-2
        <?= ($uri0 == "Usermanagement") ? ' active' : '' ?>"
      >
        <i class="bi bi-person-exclamation"></i>
        Usermanagement
      </a>
    </nav>
    <h6 class="py-2 px-3 mb-0 fw-normal text-secondary">Tutorial</h6>
    <nav class="list-group list-group-flush mb-2">
      <a
        href="/News"
        class="list-group-item list-group-item-action border-bottom-0 d-flex align-items-center gap-2
        <?= ($uri0 == "News") ? ' active' : '' ?>"
      >
        <i class="bi bi-newspaper"></i>
        News
      </a>
    </nav>
    <h6 class="py-2 px-3 mb-0 fw-normal text-secondary">User</h6>
    <nav class="list-group list-group-flush">
      <a
        href="/<?= (auth()->loggedIn()) ? 'logout' : 'login' ?>"
        class="list-group-item list-group-item-action border-bottom-0 d-flex align-items-center gap-2"
      >
        <i class="bi bi-door-<?= (auth()->loggedIn()) ? 'closed' : 'open' ?>"></i>
        Log<?= (auth()->loggedIn()) ? 'out' : 'in' ?>
      </a>
      <?php if (!auth()->loggedIn()) : ?>
          <a
          href="/register"
          class="list-group-item list-group-item-action border-bottom-0 d-flex align-items-center gap-2"
        >
          <i class="bi bi-r-circle"></i>
          Register
        </a>
      <?php endif ?>
    </nav>
  </div>
</div>
