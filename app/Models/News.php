<?php

namespace App\Models;

use CodeIgniter\Model;

class News extends Model
{
    protected $table = 'news';
    protected $primaryKey = 'id';
    protected $allowedFields = ['title', 'body'];
    protected $validationRules = [
        'title' => 'required|max_length[255]|min_length[3]',
        'body' => 'required|max_length[5000]|min_length[10]',
    ];
}
