<?php

namespace App\Models\Soap;

class DataSourceService extends AC5SoapClient
{
    public function __construct()
    {
        parent::__construct($_ENV['A5_ROOT'], 'dataSource');
    }

    /**
     * Authenticate with the given token.
     *
     * @param string $authToken
     *
     * @return bool
     */
    public function authenticate($authToken)
    {
        return parent::authenticate($authToken);
    }

    public function fetchDataSourceAll()
    {
        return parent::__call('fetchDataSourceAll', array());
    }

    public function fetchDataSourceFields($dataSourceId)
    {
        return parent::__call('fetchDataSourceFields', [$dataSourceId]);
    }
}
