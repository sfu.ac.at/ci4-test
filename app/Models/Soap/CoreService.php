<?php

namespace App\Models\Soap;

/**
 * Implementation of ac5 core service.
 *
 * @author Benedikt Schaller
 */
class CoreService extends AC5SoapClient
{
    /**
     * (non-PHPdoc).
     *
     * @see AC5SoapClient::__construct()
     *
     * @author Benedikt Schaller
     */
    public function __construct()
    {
        parent::__construct($_ENV['A5_ROOT'], 'core');
    }

    /**
     * Implement login function.
     *
     * @author Benedikt Schaller
     *
     * @return string
     */
    public function login()
    {
        return parent::login($_ENV['A5_USER'], $_ENV['A5_PASS']);
    }
}
