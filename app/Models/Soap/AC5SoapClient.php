<?php

namespace App\Models\Soap;

use SoapClient;

/**
 * Soap client to use ac5 soap service.
 *
 * @author Benedikt Schaller
 */
class AC5SoapClient extends SoapClient
{
    /**
     * (non-PHPdoc).
     *
     * @see SoapClient::SoapClient()
     *
     * @author Benedikt Schaller
     */
    public function __construct($urlRoot, $serviceName, $versionNumber = null)
    {
        // Create the url and the options
        $wsdlUrl = $urlRoot.'/soap/'.$serviceName.'?WSDL';
        if (!empty($versionNumber)) {
            $wsdlUrl .= '&version='.$versionNumber;
        }
        $soapOptions = [
            'trace' => 1,
            'exception' => 1,
            'cache_wsdl' => WSDL_CACHE_BOTH,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS | SOAP_USE_XSI_ARRAY_TYPE | SOAP_WAIT_ONE_WAY_CALLS,
            // Next two settings are very important to prevent http2 upgrades, that will kill the connection
            'stream_context' => stream_context_create(['http' => ['header' => "Upgrade: HTTP/1.1\r\n"]]),
            'keep_alive' => false,
        ];
        parent::__construct($wsdlUrl, $soapOptions);
    }

    /**
     * (non-PHPdoc).
     *
     * @see SoapClient::__call()
     *
     * @author Benedikt Schaller
     */
    public function __call(string $function_name, array $arguments): mixed
    {
        return $this->__soapCall($function_name, $arguments);
    }

    /**
     * (non-PHPdoc).
     *
     * @see SoapClient::__soapCall()
     *
     * @author Benedikt Schaller
     */
    public function __soapCall(string $function_name, array $arguments, ?array $options = null, $input_headers = null, &$output_headers = null): mixed
    {
        try {
            $result = parent::__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);

            return $this->convertResult($result);
        } catch (SoapFault $ex) {
            // We need to catch the fault for nullable values
            if ($ex->faultcode == 10000) {
                return null;
            }
            throw $ex;
        }
    }

    /**
     * Converts a result value.
     *
     * @author Benedikt Schaller
     */
    private function convertResult($result)
    {
        // An array is always returned as a variable of a stdClass Object with only one parameter
        // and here we extract this and return only the array
        if (is_object($result) && get_class($result) == 'stdClass') {
            $result = $this->convertResponseValue($result);
        } elseif (is_object($result)) {
            $reflectionObject = new ReflectionObject($result);
            foreach ($reflectionObject->getProperties() as $property) {
                /* @var $property ReflectionProperty */
                if (!$property->isPublic()) {
                    $property->setAccessible(true);
                }
                $property->setValue($result, $this->convertResult($property->getValue($result)));
            }
        }

        return $result;
    }

    /**
     * Converts the response value.
     *
     * @author tpawlow
     */
    public static function convertResponseValue($responseValue)
    {
        $objectVariables = get_object_vars($responseValue);
        // If exaclty one variable we have an array wrapper object and the variable holds the array
        if (count($objectVariables) == 1) {
            $firstValue = array_shift($objectVariables);
            if (is_array($firstValue)) {
                foreach ($firstValue as $key => $value) {
                    if (!$value instanceof stdClass) {
                        continue;
                    }
                    $firstValue[$key] = self::convertResponseValue($value);
                }

                return $firstValue;
            }
            // If no parameters present, the result schould be an empty array
        } elseif (count($objectVariables) == 0) {
            return [];
        }

        return $responseValue;
    }

    /**
     * Authenticate with the given token.
     *
     * @author Benedikt Schaller
     *
     * @param string $authToken
     *
     * @return bool
     */
    public function authenticate($authToken)
    {
        return $this->__soapCall('authenticate', [$authToken]);
    }
}
