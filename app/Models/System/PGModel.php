<?php

namespace App\Models\System;

use CodeIgniter\Model;

class PGModel extends Model
{
    public const MODEL_FILE = __DIR__ . '/../../Database/pgmodel.dbm';
    private $model;
    private $pgcatalog;
    private $check;

    protected function initialize()
    {
        if (!file_exists(self::MODEL_FILE)) {
            throw new \Exception(self::MODEL_FILE . ' is missing');
        }
        $this->model = simplexml_load_file(self::MODEL_FILE);
        $this->pgcatalog = model('App\Models\System\PGCatalog');
        $this->check = new \stdClass();
    }

    /**
     * Adds negative checks to results
     *
     * @param String $name Name of the check
     * @param String $desc Description of the check
     * @param String $status Status of the check
     * @param Array $results Negative results
     */
    private function addCheck($name, $desc, $problem, $solution, &$results)
    {
        $this->check->name = $name;
        $this->check->version =  $desc;
        $this->check->loaded = false;
        $this->check->problem = $problem;
        $this->check->solution = $solution;
        $results[] = clone $this->check;
    }

    /**
     * Checks the existence of model object in the database object names array
     *
     * @param String $mod_obj_type Type of the model object
     * @param Array $db_obj_names Database object names
     * @param Array $results Negative results
     */
    private function checkDbObject($mod_obj_type, $db_obj_names, &$results)
    {
        $mod_obj_names = array();
        // Check every model object
        foreach ($this->model->$mod_obj_type as $mod_obj) {
            $mod_obj_name = (string) $mod_obj['name'];
            $mod_obj_names[] = $mod_obj_name;
            // Check existence of object name in database
            if (!in_array($mod_obj_name, $db_obj_names)) {
                $this->addCheck(
                    $mod_obj_type,
                    $mod_obj_name,
                    $mod_obj_type . ' ' . $mod_obj_name . ' is missing in database',
                    'create ' .  $mod_obj_type . ' ' . $mod_obj_name,
                    $results
                );
            }
        }
        // Check every database object
        foreach ($db_obj_names as $db_obj_name) {
            // Check existence of object name in model
            if (!in_array($db_obj_name, $mod_obj_names)) {
                $this->addCheck(
                    $mod_obj_type,
                    $db_obj_name,
                    $mod_obj_type . ' ' . $db_obj_name . ' is missing in model',
                    'create ' .  $mod_obj_type . ' ' . $db_obj_name,
                    $results
                );
            }
        }
    }

    /**
     * Checks the existence of schemas in the model and database
     *
     * @param Array $results Negative results
     */
    public function checkSchema(&$results)
    {
        $db_obj_names = $this->pgcatalog->getSchemaNames();
        $this->checkDbObject('schema', $db_obj_names, $results);
    }

    /**
     * Checks the existence of indexes in the model and database
     *
     * @param Array $results Negative results
     */
    public function checkIndex(&$results)
    {
        $db_obj_names = $this->pgcatalog->getIndexNames();
        $this->checkDbObject('index', $db_obj_names, $results);
    }

    /**
     * Checks the existence and datatype of columns in the model and database
     *
     * @param SimpleXMLElement $table Table Object
     * @param String $table_name Name of the table
     * @param String $schema_name Name of the schema
     * @param Array $results Negative results
     */
    public function checkColumn($table, $table_name, $schema_name, &$results)
    {
        $mod_obj_names = array();
        $mod_obj_type = 'column';
        $column_names_types = $this->pgcatalog->getColumnNamesTypes($table_name, $schema_name);
        // Check every model column
        foreach ($table->column as $mod_obj) {
            $mod_obj_name = (string) $mod_obj['name'];
            $mod_obj_desc = $schema_name . '.' . $table_name . '.' . $mod_obj_name;
            $mod_col_type = (string) $mod_obj->type[0]['name'];
            $db_col_type = array_key_exists($mod_obj_name, $column_names_types) ? $column_names_types[$mod_obj_name] : null;
            $mod_obj_names[] = $mod_obj_name;
            // Check existence of column in database
            if (!array_key_exists($mod_obj_name, $column_names_types)) {
                $this->addCheck(
                    $mod_obj_type,
                    $mod_obj_desc,
                    $mod_obj_type . ' ' . $mod_obj_desc . ' is missing in database',
                    'create ' .  $mod_obj_type . ' ' . $mod_obj_desc,
                    $results
                );
            }
            // Check datatype of column in database
            elseif (
                $mod_col_type != $db_col_type
                && !(($mod_col_type == 'character varying' || $mod_col_type == 'text') && (int) $mod_obj->type[0]['dimension'] == 1 && $db_col_type == 'ARRAY')
                && !($mod_col_type == 'timestamp' && $db_col_type == 'timestamp without time zone')
                && !($mod_col_type == 'time' && $db_col_type == 'time without time zone')
            ) {
                $this->addCheck(
                    'datatype',
                    $mod_obj_desc,
                    'model datatype: ' . $mod_col_type . ' != database datatype: ' . $db_col_type,
                    'change model or database datatype in ' . $mod_obj_desc,
                    $results
                );
            }
        }
        // Check every database column
        foreach (array_keys($column_names_types) as $db_obj_name) {
            $db_obj_desc = $schema_name . '.' . $table_name . '.' . $db_obj_name;
            // Check existence of column in model
            if (!in_array($db_obj_name, $mod_obj_names)) {
                $this->addCheck(
                    $mod_obj_type,
                    $db_obj_desc,
                    $mod_obj_type . ' ' . $db_obj_desc . ' is missing in model',
                    'create ' .  $mod_obj_type . ' ' . $db_obj_desc,
                    $results
                );
            }
        }
    }

    /**
     * Checks the existence of tables and columns in the model and database
     *
     * @param Array $results Negative results
     */
    public function checkTableColumn(&$results)
    {
        $mod_obj_names = array();
        $mod_obj_type = 'table';
        $db_obj_names = $this->pgcatalog->getTableNames();
        // Check every model table
        foreach ($this->model->$mod_obj_type as $mod_obj) {
            $mod_obj_name = (string) $mod_obj['name'];
            $schema_name = (string) $mod_obj->schema[0]['name'];
            $mod_obj_names[] = $mod_obj_name;
            // Check existence of table in database
            if (!in_array($mod_obj_name, $db_obj_names)) {
                $this->addCheck(
                    $mod_obj_type,
                    $schema_name . '.' . $mod_obj_name,
                    $mod_obj_type . ' ' . $schema_name . '.' . $mod_obj_name . ' is missing in database',
                    'create ' .  $mod_obj_type . ' ' . $schema_name . '.' . $mod_obj_name,
                    $results
                );
            } else {
                $this->checkColumn($mod_obj, $mod_obj_name, $schema_name, $results);
            }
        }
        // Check every database table
        foreach ($db_obj_names as $db_obj_name) {
            // Check existence of table in model
            if (!in_array($db_obj_name, $mod_obj_names)) {
                $this->addCheck(
                    $mod_obj_type,
                    $db_obj_name,
                    $mod_obj_type . ' ' . $db_obj_name . ' is missing in model',
                    'create ' .  $mod_obj_type . ' ' . $db_obj_name,
                    $results
                );
            }
        }
    }
}
