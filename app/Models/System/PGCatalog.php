<?php

namespace App\Models\System;

use CodeIgniter\Model;

class PGCatalog extends Model
{

  /**
   * Query information_schema.schemata
   * 
   * @return Array Schema names
   */
  public function getSchemaNames()
  {
    $sql = "SELECT schema_name
      FROM information_schema.schemata
      WHERE schema_owner != 'postgres'";
    $query = $this->db->query($sql);
    return array_column($query->getResult(), 'schema_name');
  }

  /**
   * Query pg_indexes
   * 
   * @return Array Index names
   */
  public function getIndexNames()
  {
    $sql = "SELECT indexname
        FROM pg_indexes i
        LEFT JOIN pg_constraint c ON i.indexname = c.conname
        WHERE i.schemaname != 'pg_catalog' AND c.conname IS NULL";
    $query = $this->db->query($sql);
    return array_column($query->getResult(), 'indexname');
  }

  /**
   * Query information_schema.tables
   * 
   * @return Array Table names
   */
  public function getTableNames()
  {
    $sql = "SELECT table_name
        FROM information_schema.tables
        WHERE table_type != 'VIEW'
        AND table_schema != 'pg_catalog'
        AND table_schema != 'information_schema'";
    $query = $this->db->query($sql);
    return array_column($query->getResult(), 'table_name');
  }

  /**
   * Query information_schema.columns
   * 
   * @param String $table_name Name of the table
   * @param String $schema_name Name of the schema
   * @return Array Key: column name Value: column type
   */
  public function getColumnNamesTypes($table_name, $schema_name)
  {
    $sql = "SELECT column_name, data_type
        FROM information_schema.columns
        WHERE table_name = '" . $table_name . "'
        AND table_schema = '" . $schema_name . "'";
    $query = $this->db->query($sql);
    $result = $query->getResult();
    return array_combine(array_column($result, 'column_name'), array_column($result, 'data_type'));
  }
}