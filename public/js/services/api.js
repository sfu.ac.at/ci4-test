export async function fetchPost(url, data) {
  const result = {};
  const options = {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "Content-type": "application/json; charset=utf-8" },
  };
  try {
    const response = await fetch(url, options);
    const json = await response.json();
    result[response.ok ? "data" : "error"] = json;
  } catch (error) {
    result.error = error;
  }
  return result;
}
export async function fetchDelete(url) {
  const result = {};
  const options = { method: "DELETE" };
  try {
    const response = await fetch(url, options);
    const json = await response.json();
    result[response.ok ? "data" : "error"] = json;
  } catch (error) {
    result.error = error;
  }
  return result;
}
