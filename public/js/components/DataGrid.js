// https://vuejs.org/examples/#grid
const { computed, reactive } = Vue
function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}
function generateHeaders(data) {
  return data.length ? Object.keys(data[0]).map((key) => ({ key, label: capitalize(key) })) : []
}
function getExcludedKeys(data, headers) {
  const headerKeys = new Set(headers.map((h) => h.key))
  return Object.keys(data[0]).filter((key) => !headerKeys.has(key))
}
function filterData(data, key, ignore = []) {
  if (!key || !data || !data.length) return data
  const filterKey = key.toLowerCase()
  const ignoreSet = new Set(ignore)
  return data.filter((row) => {
    return Object.keys(row).some((field) => {
      return !ignoreSet.has(field) && String(row[field]).toLowerCase().indexOf(filterKey) > -1
    })
  })
}
function sortData(data, key, orders) {
  if (!key || !data || !data.length || !orders) return data
  const order = orders[key]
  return data.slice().sort((a, b) => {
    a = a[key]
    b = b[key]
    return (a === b ? 0 : a > b ? 1 : -1) * order
  })
}
export default {
  name: 'DataGrid',
  props: {
    data: {
      type: Array,
      required: true
    },
    headers: Array,
    filterKey: {
      type: String,
      default: ''
    },
    noResults: {
      type: String,
      default: 'No results.'
    }
  },
  emits: ['selectItem'],
  setup(props) {
    const columns = props.headers?.length ? props.headers : generateHeaders(props.data)
    const excludedKeys =
      props.data.length && props.headers?.length ? getExcludedKeys(props.data, columns) : []
    const sort = reactive({
      key: '',
      orders: columns.reduce((o, column) => {
        o[column.key] = 1
        return o
      }, {}),
      sortBy(key) {
        this.key = key
        this.orders[key] *= -1
      }
    })
    const rows = computed(() => {
      if (!props.data.length) return []
      const filtered = filterData(props.data, props.filterKey, excludedKeys)
      return sortData(filtered, sort.key, sort.orders)
    })
    return { sort, rows, columns }
  },
  template: /*html*/ `
    <div v-if="rows.length" class="table-responsive">
      <table class="table table-bordered table-hover">
          <thead>
              <tr>
            <th v-for="column in columns" :key="column.key" class="text-nowrap" @click="column.sortable !== false && sort.sortBy(column.key)">
              <span class="me-2">{{ column.label }}</span>
              <i v-if="column.sortable !== false" class="bi" :class="sort.orders[column.key] > 0 ? 'bi-arrow-up' : 'bi-arrow-down'"></i>
                  </th>
              </tr>
          </thead>
          <tbody>
          <tr v-for="row in rows" :key="row[columns[0].key]" @click="$emit('selectItem', row)">
            <td v-for="(column, index) in columns" :key="index">{{ row[column.key] }}</td>
              </tr>
          </tbody>
      </table>
  </div>
    <p v-else class="text-center">{{ noResults }}</p>
  `
}
